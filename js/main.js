const board = document.querySelector('.board');
let head, bodyS, tail, snakeAllBody;
      
const createBoard = (() => {
    for (let i = 0; i < 100; i++) {
        let squre = document.createElement('div');
        squre.classList.add('squre');
        board.appendChild(squre);
    }    
})();

const boardSqure = Array.from(document.querySelectorAll('.squre'));

const getRandomElement = () => {
    return boardSqure.splice(Math.floor(Math.random() * boardSqure.length), 1)[0];
};

const addSneck = () => {
    let randomItem = getRandomElement();
    randomItem.classList.add('sneck');

    let snakeAndSneck = Array.from(document.getElementsByClassName('body-s sneck'));

    snakeAndSneck.forEach(item => {
        item.classList.remove('sneck'); 
        addSneck();

    }); 
};

const addCoards = () => {
    let x = 1, 
        y = 10;

    for(let i = 0; i < boardSqure.length; i++) {
        if(x > 10) {
            x = 1;
            y--;
        }

        boardSqure[i].setAttribute('data-x', x);
        boardSqure[i].setAttribute('data-y', y);
        x++;
    }

    getCoordSnake();
};

const randomPositionSnake = (min = 1, max = 10) => {
    let posX = Math.floor(Math.random() * (max - (min + 2)) + 3);
    let posY = Math.floor(Math.random() * (max - min) + min);
    return [posX, posY];
};

const getCoordSnake = () => {
    let coordinates = randomPositionSnake();
    let snakeBody = [
        document.querySelector('[data-x="' +  coordinates[0] + '"]' + '[data-y="' +  coordinates[1] + '"]'),
        document.querySelector('[data-x="' +  (coordinates[0] - 1) + '"]' + '[data-y="' +  coordinates[1] + '"]'),
        document.querySelector('[data-x="' +  (coordinates[0] - 2) + '"]' + '[data-y="' +  coordinates[1] + '"]'),
    ];
    snakeAllBody = snakeBody
    addSnakeBody(snakeBody);
};



const addSnakeBody = bodyArr => {
    let counter = bodyArr.length - 1;

    head = bodyArr[0];
    bodyS = bodyArr.slice(1, counter++);
    tail = bodyArr[bodyArr.length - 1];

    const createSnakeHead = (() => {
        head.classList.add('body-s', 'head-snake');
    })();

    const createSnakeBody = (() => {
        bodyS.forEach(item => {
            item.classList.add('body-s', 'body-snake');
        });  
    })();

    const createSnakeTail = (() => {
        tail.classList.add('body-s', 'tail');
    })();
};

let counterHead = 0,
    counterBody = 0,
    counterTail = 0;

let newHeadCoord;




const moveSnake = () => {
    let coordHead = [head.getAttribute('data-x'), head.getAttribute('data-y')],
        coordTail = [tail.getAttribute('data-x'), tail.getAttribute('data-y')],
        coordBodySnake,
        snakeBodyItem;

    
    bodyS.forEach(item => {
        snakeBodyItem = item;
        coordBodySnake = [item.getAttribute('data-x'), item.getAttribute('data-y')];
    });

    boardSqure.forEach(item => {
        item.classList.remove('body-s', 'head-snake');
        item.classList.remove('body-s', 'body-snake');
        item.classList.remove('body-s', 'tail');
    });


    let newHead = document.querySelector('[data-x="' +  (+coordHead[0] + counterHead++) + '"]' + '[data-y="' +  coordHead[1] + '"]'),
        newBody = document.querySelector('[data-x="' +  (+coordBodySnake[0] + counterBody++) + '"]' + '[data-y="' +  coordBodySnake[1] + '"]'),
        newTail = document.querySelector('[data-x="' +  (+coordTail[0] + counterTail++) + '"]' + '[data-y="' +  coordTail[1] + '"]');

        newHeadCoord = [newHead.getAttribute('data-x'), newHead.getAttribute('data-y')];
    

    if(newHeadCoord[0] <= 10) {
        newHead.classList.add('body-s', 'head-snake');
        newBody.classList.add('body-s', 'body-snake'); 
        newTail.classList.add('body-s', 'tail');
    }  
};


const snecks = () => {
    addSneck();
    addSneck();
};

addCoards();
snecks();
moveSnake();



let interval = setInterval(() => {
    moveSnake();
}, 1000);